<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class AuthController extends Controller
{

	public function __construct()
    {
    }
	public function register(Request $request)
	{
		$this->validate($request, [
			'name' => 'required',
			'username' => 'required|unique:users|min:6',
			'password' => 'required|min:6',
		]);
		$name = $request->input('name');
		$username = $request->input('username');
		$password = $request->input('password');

		$hashPwd = Hash::make($password);

		$data_insert = ([
			'name' => $name,
			'username' => $username,
			'password' => $hashPwd
		]);

		// $save = User::create($data_insert);
		$dataUser = new User();
		$dataUser->name = $name;
		$dataUser->username = $username;
		$dataUser->password = $hashPwd;

		if ($dataUser->save()) {
			$out = [
				'message' => 'register_success',
				'code' => '200',
			];
		} else {
			$out = [
				'message' => 'failed_register',
				'code' => '404',
			];
		}

		return response()->json($out, $out['code']);
	}
	public function login(Request $request)
	{
		$this->validate($request, [
			'username' => 'required',
			'password' => 'required'
		]);

		$username = $request->input('username');
		$password = $request->input('password');

		$user = User::where("username", $username)->first();

		if($user) {
			$check_password = Hash::check($password, $user->password);

			if($check_password) {
				$new_token = $this->generateRandomString();

				$create_token = User::where('id', $user->id)->update(['token' => $new_token]);
				if($create_token) {
					$output = [
						'message' => 'Login_success',
						'code' => '200',
						'result' => [
							'token' => $new_token
						]
					];
				} else {
					$output = [
						'message' => 'Login_not_success',
						'code' => '401',
					];
				}
			} else {
				$output = [
					'message' => 'Password_not_valid',
					'code' => '401',
				];
			}
		} else {
			$output = [
				'message' => 'User_not_valid',
				'code' => '401',
			];
		}

		return response()->json($output, $output['code']);
	}
	public function generateRandomString($length = 80)
	{
		$chars = '012345678dssd9abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$char_length = strlen($chars);
		$str = '';
		for ($i = 0; $i < $length; $i++) {
			$str .= $chars[rand(0, $char_length - 1)];
		}
		return $str;
	}
}
?>