<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class CheckAdmin
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard($guard)->guest()) {
            if($request->has('token')) {
                $token = $request->request('Authorization');
                $check_token = User::where('token', $token)->first();
                if(!$check_token) {
                    return response()->json([
                        'message' => 'permission_not_allowed',
                        'code' => 401
                    ], 401);
                }
            } else {
                return response()->json([
                    'message' => 'token_nothing',
                    'code' => 401
                ], 401);
            }
        }

        return $next($request);
    }
}
