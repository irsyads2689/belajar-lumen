<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{

	public function index()
	{
		$users = User::all();

		$output = [
			'result' => $users,
			'code' => '200',
		];
		return response()->json($output, $output['code']);
	}

	// public function detail($id)
	// {
	// 	$user = User::find($id);

	// 	$output = [
	// 		'result' => $user,
	// 		'code' => '200',
	// 	];
	// 	return response()->json($output, $output['code']);
	// }
}
?>